﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.PostgreSqlContext
{
    public static class EFInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<PostgresqlContext>(optionsBuilder
                => optionsBuilder
                    .UseLazyLoadingProxies()
       				.UseNpgsql(connectionString));



            //#region health checks

            //services.AddHealthChecks()
            //    .AddDbContextCheck<PostgresqlContext>(
            //        tags: new[] { "db_ef_healthcheck" },
            //        customTestQuery: async (context, token) =>
            //        {
            //            return await context.Lessons.AnyAsync(token);
            //        });

            //#endregion

            return services;
        }
    }
}
