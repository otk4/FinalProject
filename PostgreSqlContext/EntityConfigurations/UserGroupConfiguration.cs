﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class UserGroupConfiguration : IEntityTypeConfiguration<UserGroup>
    {
        public void Configure(EntityTypeBuilder<UserGroup> builder)
        {
            builder.HasKey(re => new { re.UserId, re.GroupId });
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);

            builder.HasOne(s => s.Group).WithMany(r => r.Users).HasForeignKey(s => s.GroupId).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(s => s.User).WithMany(r => r.Groups).HasForeignKey(s => s.UserId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
