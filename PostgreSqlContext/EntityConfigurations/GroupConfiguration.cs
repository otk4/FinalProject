﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.HasKey(approach => approach.Id);
            builder.HasIndex(approach => approach.Id);
            builder.HasIndex(approach => approach.Name);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);
        }
    }
}
