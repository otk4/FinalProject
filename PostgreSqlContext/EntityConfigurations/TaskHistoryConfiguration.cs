﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class TaskHistoryConfiguration : IEntityTypeConfiguration<TaskHistory>
    {
        public void Configure(EntityTypeBuilder<TaskHistory> builder)
        {
            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);

            builder.HasOne(s => s.Entity).WithMany(s => s.History).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(s => s.User);
        }
    }
}
