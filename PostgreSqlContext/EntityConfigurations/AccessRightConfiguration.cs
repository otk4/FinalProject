﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class AccessRightConfiguration : IEntityTypeConfiguration<AccessRight>
    {
        public void Configure(EntityTypeBuilder<AccessRight> builder)
        {
            builder.HasKey(approach => approach.Id);
            builder.HasIndex(approach => approach.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);

        }
    }
}
