﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class TaskItemConfiguration : IEntityTypeConfiguration<TaskItem>
    {
        public void Configure(EntityTypeBuilder<TaskItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);
            builder.Property(s => s.Deleted).HasDefaultValue(false).ValueGeneratedOnAdd().IsRequired(true);

            //builder.HasMany(u => u.History).WithOne(c => c.Entity).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(p => p.Project).WithMany(t => t.Tasks).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(s => s.Responcible);
        }
    }
}
