﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);
            builder.Property(s => s.Deleted).HasDefaultValue(false).ValueGeneratedOnAdd().IsRequired(true);
            builder.HasMany(u => u.Tasks).WithOne(c => c.Project).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
