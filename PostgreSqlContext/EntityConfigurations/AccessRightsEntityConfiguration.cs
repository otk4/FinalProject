﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext.EntityConfigurations
{
    public class AccessRightsEntityConfiguration : IEntityTypeConfiguration<AccessRightsEntity>
    {
        public void Configure(EntityTypeBuilder<AccessRightsEntity> builder)
        {
            builder.HasKey(approach => approach.Id);
            builder.HasIndex(approach => approach.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd().IsRequired(true);
        }
    }
}
