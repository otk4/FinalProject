﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PostgreSqlContext.EntityConfigurations;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.PostgreSqlContext
{
    public class PostgresqlContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public PostgresqlContext(DbContextOptions<PostgresqlContext> options, IConfiguration configuration)
        : base(options)
        {
            Configuration = configuration;
            Database.EnsureCreated();
        }

        public DbSet<TaskItem> TaskItems { get; set; }
        public DbSet<TaskHistory> TaskHistories { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Domain.Entities.Group> Groups { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<AccessRight> AccessRights { get; set; }
        public DbSet<AccessRightsEntity> AccessRightsEntities { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration())
                        .ApplyConfiguration(new GroupConfiguration())
                        .ApplyConfiguration(new UserGroupConfiguration())
                        .ApplyConfiguration(new AccessRightConfiguration())
                        .ApplyConfiguration(new AccessRightsEntityConfiguration())
                        .ApplyConfiguration(new TaskItemConfiguration())
                        .ApplyConfiguration(new TaskHistoryConfiguration())
                        .ApplyConfiguration(new ProjectConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(Configuration.GetConnectionString("PG"));
            optionsBuilder
                .UseLazyLoadingProxies()
                .LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}