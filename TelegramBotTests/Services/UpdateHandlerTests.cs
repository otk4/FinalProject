﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Services.Tests
{
    [TestClass()]
    public class UpdateHandlerTests
    {
        [TestMethod()]
        public void PrepareMessageTest()
        {
            var msg = new Telegram.Bot.Types.Message();
            msg.From = new Telegram.Bot.Types.User();
            msg.From.Id = 1;
            UpdateHandler.PrepareMessage(msg);
            Assert.AreEqual("Your ID: 1", UpdateHandler.PrepareMessage(msg));
        }
    }
}