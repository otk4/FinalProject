using MassTransit;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Telegram.Bot;
using TelegramBot.Services;
using TelegramBot.Settings;

namespace TelegramBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers();

            var rmqConfigSection = builder.Configuration.GetSection("RMQSettings");
            builder.Services.Configure<RmqSettings>(rmqConfigSection);
            var rmqSettings = rmqConfigSection.Get<RmqSettings>();

            builder.Services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    Console.WriteLine($"PLAZMED {rmqSettings.Host} | {rmqSettings.VHost} | {rmqSettings.Username} | {rmqSettings.Password}");
                    cfg.Host(rmqSettings.Host, rmqSettings.VHost, h =>
                    {
                        h.Username(rmqSettings.Username);
                        h.Password(rmqSettings.Password);
                    });
                    cfg.ConfigureEndpoints(context);
                    cfg.ReceiveEndpoint("telegram_event", e =>
                    {
                        e.Consumer<TelegramEventConsumer>();
                    });
                });
            });

            builder.Services.Configure<MongoSettings>(builder.Configuration.GetSection("DatabaseSettings"));

            builder.Services.AddSingleton<IMongoClient>(sp =>
            {
                var settings = sp.GetRequiredService<IOptions<MongoSettings>>().Value;
                return new MongoClient(settings.ConnectionString);
            });

            var botConfigSection = builder.Configuration.GetSection("BotConfiguration");
            builder.Services.Configure<BotConfiguration>(botConfigSection);
            builder.Services.AddHttpClient("tgwebhook").RemoveAllLoggers().AddTypedClient<ITelegramBotClient>(
                httpClient => new TelegramBotClient(botConfigSection.Get<BotConfiguration>()!.BotToken, httpClient));
            builder.Services.AddSingleton<UpdateHandler>();
            builder.Services.ConfigureTelegramBotMvc();

            var app = builder.Build();
            app.UseAuthorization();
            app.MapControllers();

            app.UseMiddleware<Middleware.MongoDbLoggingMiddleware>();

            app.Run();
        }
    }
}
