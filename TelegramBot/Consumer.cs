﻿using MassTransit;
using Domain.Entities;

namespace TelegramBot
{
    public class TelegramEventConsumer : IConsumer<TelegramEvent>
    {
        public async Task Consume(ConsumeContext<TelegramEvent> context)
        {
            // Handle the message
            var message = context.Message;
            // Process the message (e.g., log it, save to database, etc.)
            Console.WriteLine($"Received message: {message.Message}");
            await Task.CompletedTask;
        }
    }
}
