using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace TelegramBot.Middleware
{
    public class MongoDbLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMongoCollection<LogMessage> _logCollection;

        public MongoDbLoggingMiddleware(RequestDelegate next, IMongoClient mongoClient, IOptions<MongoSettings> dbSettings)
        {
            _next = next;
            var database = mongoClient.GetDatabase(dbSettings.Value.Database);
            _logCollection = database.GetCollection<LogMessage>("LogMessages");
        }

        public async Task InvokeAsync(HttpContext context)
        {
            // Capture the message you want to log
            var message = $"Request made to: {context.Request.Path}";

            // Save the message to MongoDB
            await _logCollection.InsertOneAsync(new LogMessage { Message = message });

            // Call the next middleware in the pipeline
            await _next(context);
        }
    }

    public class LogMessage
    {
        public string Message { get; set; }
    }
}