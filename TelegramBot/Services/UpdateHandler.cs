using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TelegramBot.Services;

public class UpdateHandler(ITelegramBotClient bot, ILogger<UpdateHandler> logger) : IUpdateHandler
{
    public async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, HandleErrorSource source, CancellationToken cancellationToken)
    {
        logger.LogInformation("HandleError: {Exception}", exception);
        // Cooldown in case of network connection error
        if (exception is RequestException)
            await Task.Delay(TimeSpan.FromSeconds(2), cancellationToken);
    }

    public async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        await (update switch
        {
            { Message: { } message } => OnMessage(message),
            _ => UnknownUpdateHandlerAsync(update)
        });
    }

    private async Task OnMessage(Message msg)
    {
        logger.LogInformation("Receive message type: {MessageType}", msg.Type);
        if (msg.Text is not { } messageText)
            return;

        if (msg.Text == "/start")
        {
            var sentMessage = await SendInfo(msg);
            logger.LogInformation("The message was sent with id: {SentMessageId}", sentMessage.MessageId);
        }
    }

    /// <summary>
    /// ������������ ������������� ���, ������� ����� �������������� 
    /// </summary>
    /// <param name="msg"></param>
    /// <returns></returns>
    public static string PrepareMessage(Message msg)
    {
        return $"Your ID: {msg.From.Id}";
    }

    /// <summary>
    /// ��������� ��������� � ����������� � ������������
    /// </summary>
    /// <param name="msg"></param>
    /// <returns></returns>
    public async Task<Message> SendInfo(Message msg)
    {
        string usage = $"Your ID: {msg.From.Id}";
        return await bot.SendTextMessageAsync(msg.Chat, usage, parseMode: ParseMode.Html);
    }

    private Task UnknownUpdateHandlerAsync(Update update)
    {
        logger.LogInformation("Unknown update type: {UpdateType}", update.Type);
        return Task.CompletedTask;
    }
}
