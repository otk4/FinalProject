﻿using AutoMapper;
using FinalProject.Models.UserModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts.User;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Контроллер пользователей
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _logger;
        private readonly IHttpContextAccessor _accessor; 

        public UserController(IUserService service, ILogger<UserController> logger, IMapper mapper, IHttpContextAccessor accessor)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
            _accessor = accessor;
        }

        /// <summary>
        /// Получение пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Дто пользователя</returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            return Ok(_mapper.Map<UserModel>(await _service.GetByIdAsync(id)));
        }

        /// <summary>
        /// Создание пользователя
        /// </summary>
        /// <param name="usermodel">Дто создаваемого пользователя</param>
        /// <returns>Идентификатор созданного пользователя</returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateAsync(UserModel usermodel)
        {
            return Ok(await _service.CreateAsync(_mapper.Map<UserDto>(usermodel)));
        }


        /// <summary>
        /// Редактирование пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="userModel">Дто с данными для редактирования</param>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> EditAsync(int id, UserModel userModel)
        {
            await _service.UpdateAsync(id, _mapper.Map<UserModel, UserDto>(userModel));
            return Ok();
        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        [Authorize]
        [HttpDelete("{id}")]
        public void DeleteAsync(int id)
        {
            _service.Delete(id);
        }


        /// <summary>
        /// Получение списка пользователей
        /// </summary>
        /// <param name="token">ТОкен отмены выполнения</param>
        /// <returns>Список пользователей</returns>
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync(CancellationToken token)
        {
            return Ok(await _service.GetAllAsync(token));
        }

        #region auth

        /// <summary>
        /// Аутентификация
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("login")]
        public async Task<IActionResult> Login(string login, string pass, CancellationToken token)
        {
            var tkn = await _service.Login(login, pass);
            _accessor.HttpContext?.Response.Cookies.Append("asaru", tkn);
            return Ok(tkn);
        }

        #endregion auth
    }
}
