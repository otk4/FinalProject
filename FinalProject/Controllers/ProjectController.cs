﻿using AutoMapper;
using FinalProject.Models.ProjectModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts.Project;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Контроллер проектов
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<ProjectController> _logger;

        public ProjectController(IProjectService service, ILogger<ProjectController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение проекта по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор проекта</param>
        /// <returns>Дто проекта</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            return Ok(_mapper.Map<ProjectModel>(await _service.GetByIdAsync(id)));
        }

        /// <summary>
        /// Получение всего списка проектов
        /// </summary>
        /// <returns>Список дто проектов</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _service.GetAllAsync());
        }

        /// <summary>
        /// Создание проекта
        /// </summary>
        /// <param name="projectModel">Дто создаваемого проекта</param>
        /// <returns>Идентификатор созданного проекта</returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateProjectDto projectModel)
        {
            return Ok(await _service.CreateAsync(projectModel));
        }

        /// <summary>
        /// Редактирование проекта
        /// </summary>
        /// <param name="id">Идентификатор проекта</param>
        /// <param name="projectModel">Дто редактируемого проекта</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditAsync(int id, ProjectModel projectModel)
        {
            await _service.UpdateAsync(id, _mapper.Map<ProjectModel, ProjectDto>(projectModel));
            return Ok();
        }

        /// <summary>
        /// Удаление проекта
        /// </summary>
        /// <param name="id">Идентификатор удаляемого проекта</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }
    }
}
