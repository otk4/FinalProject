﻿using AutoMapper;
using FinalProject.Models.TaskModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts.Task;


namespace FinalProject.Controllers
{
    /// <summary>
    /// Контроллер задач
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<TaskController> _logger;

        public TaskController(ITaskService service, ILogger<TaskController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение задачи по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        /// <returns>Дто задачи</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            return Ok(await _service.GetByIdAsync(id));
        }

        /// <summary>
        /// Создание задачи
        /// </summary>
        /// <param name="taskModel">Дто создаваемой задачи</param>
        /// <returns>Идентификатор созданной задачи</returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreatingTaskModel taskModel)
        {
            return Ok(await _service.CreateAsync(_mapper.Map<CreatingTaskDto>(taskModel)));
        }

        /// <summary>
        /// Редактирование задачи
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        /// <param name="taskModel">Дто редактируемой задачи</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditAsync(int id, UpdatingTaskModel taskModel)
        {
            _logger.LogDebug("UpdatingTaskModel in contoller:" + System.Text.Json.JsonSerializer.Serialize(taskModel));
            await _service.UpdateAsync(id, _mapper.Map<UpdatingTaskModel, UpdatingTaskDto>(taskModel));
            return Ok();
        }

        /// <summary>
        /// Удаление задачи
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }

        /// <summary>
        /// Получение списка задач
        /// </summary>
        /// <param name="taskFilterModel">Дто фильтра</param>
        /// <returns>Список дто задач</returns>
        [HttpPost("projectTaskList")] //браузеры запрещают body для get
        public async Task<IActionResult> GetTaskListAsync(TaskFilterModel taskFilterModel)
        {
            var filterDto = _mapper.Map<TaskFilterModel, TaskFilterDto>(taskFilterModel);
            return Ok(await _service.GetTaskListAsync(filterDto));
        }
    }
}
