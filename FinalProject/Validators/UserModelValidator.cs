﻿using FinalProject.Models.UserModels;
using FluentValidation;

namespace FinalProject.Validators
{
    public class UserModelValidator : AbstractValidator<UserModel>
    {
        public UserModelValidator()
        {
            RuleFor(user => user.Name)
                .NotEmpty()
                .WithMessage("ФИО не должно быть пустым");

            RuleFor(user => user.Login)
                .NotEmpty()
                    .WithMessage("Логин не должен быть пуст")
                .MinimumLength(3)
                    .WithMessage("Логин должен быть длинее трех символов");

            RuleFor(user => user.Password)
                .NotEmpty()
                .WithMessage("Пароль не должен быть пуст133337");

            RuleFor(user => user.Email)
                .NotEmpty()
                    .WithMessage("Email не должен быть пуст")
                .Matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
                    .WithMessage("Email должен соотвествовать формату [текст@текст.домен]");

            RuleFor(user => user.PrivatePhone)
                .Must(p => p.StartsWith("+7") && p.Length == 12)
                .When(p => !string.IsNullOrEmpty(p.PrivatePhone))
                    .WithMessage("Телефон должен начинаться с +7, не содержать пробелов и спец символов и иметь длинну 12 символов");

            RuleFor(user => user.WorkPhone)
                .Must(p => p.StartsWith("+7") && p.Length == 12)
                .When(p => !string.IsNullOrEmpty(p.WorkPhone))
                    .WithMessage("Телефон должен начинаться с +7, не содержать пробелов и спец символов и иметь длинну 12 символов");

        }
    }
}
