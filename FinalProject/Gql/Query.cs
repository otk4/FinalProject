﻿using Services.Abstractions;
using Services.Contracts.User;

namespace FinalProject.Gql
{
    public class Query
    {
        private readonly IHttpContextAccessor _accessor;
        //private readonly IMapper _mapper;

        public Query(IHttpContextAccessor accessor/*, IMapper mapper*/)
        {
            _accessor = accessor;
            //_mapper = mapper;
        }

        [GraphQLNonNullType]
        public List<SimpleUserDto> GetUsers([Service] IUserService uService) => uService.GetAllAsync(_accessor.HttpContext.RequestAborted).Result.ToList();
    }
}
