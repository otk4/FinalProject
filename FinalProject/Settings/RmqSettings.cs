﻿namespace FinalProject.Settings
{
    public class RmqSettings
    {
        public string Host { get; set; }
        public string VHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
