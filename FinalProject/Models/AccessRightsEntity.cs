﻿using FinalProject.Models.UserModels;

namespace FinalProject.Models
{
    /// <summary>
    /// Модель прав на сущность.
    /// </summary>
    public class AccessRightsEntity
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Права доступа.
        /// </summary>
        public AccessRightModel AccessRight { get; set; }

        /// <summary>
        /// Субъект прав.
        /// </summary>
        public UserModel Owner { get; set; }

        /// <summary>
        /// Тип прав.
        /// </summary>
        public int AccessRightType { get; set; }
    }
}
