﻿namespace FinalProject.Models.UserModels
{
    public class SImpleUserModel
    {
        /// <summary>
        /// Идентификатор юзера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

    }
}
