﻿using FinalProject.Models.TaskModels;

namespace FinalProject.Models.ProjectModels
{
    public class ProjectModel
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Список задач.
        /// </summary>
        public List<TaskModel> Tasks { get; set; }
    }
}
