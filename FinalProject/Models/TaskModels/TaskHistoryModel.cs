﻿using Domain.Entities;
using FinalProject.Models.UserModels;

namespace FinalProject.Models.TaskModels
{
    public class TaskHistoryModel
    {
        public UserModel? User { get; set; }
        public HistoryAction Action { get; set; }
        public string Operation { get; set; }
        public DateTime OperationDate { get; set; }
        public string? Comment { get; set; }
    }
}
