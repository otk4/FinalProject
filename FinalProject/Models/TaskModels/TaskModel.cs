﻿using Domain.Entities;
using FinalProject.Models.ProjectModels;
using FinalProject.Models.UserModels;
using Services.Contracts.TaskHistory;

namespace FinalProject.Models.TaskModels
{
    public class TaskModel
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст задачи.
        /// </summary>
        public string ActiveText { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public string Priority { get; set; }

        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public string Responcible { get; set; }

        /// <summary>
        /// Проект.
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// История задачи.
        /// </summary>
        public List<TaskHistoryDto> History { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public string State { get; set; }
    }
}
