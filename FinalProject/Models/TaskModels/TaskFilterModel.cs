﻿namespace FinalProject.Models.TaskModels
{
    public class TaskFilterModel
    {
        public int ProjectId { get; set; }
        public int UserId { get; set; }
    }
}
