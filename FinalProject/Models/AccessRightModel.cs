﻿namespace FinalProject.Models
{
    /// <summary>
    /// Модель прав.
    /// </summary>
    public class AccessRightModel
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ид объекта прав.
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Тип сущности.
        /// </summary>
        public Guid EntityTypeGuid { get; set; }
    }
}
