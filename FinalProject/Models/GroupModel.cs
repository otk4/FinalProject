﻿using FinalProject.Models.UserModels;

namespace FinalProject.Models
{
    /// <summary>
    /// Модель группы.
    /// </summary>
    public class GroupModel
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Пользователи.
        /// </summary>
        public int UsersId { get; set; }
    }
}
