using AutoMapper;
using FinalProject.Gql;
using FinalProject.Mapping;
using FinalProject.Settings;
using MassTransit;
using FinalProject.Validators;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using NLog.Web;
using Services.Abstractions;
using Services.Implementations;
using System.Reflection;
using System.Text;
using HotChocolate;
using HotChocolate.AspNetCore;

namespace FinalProject
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var services = builder.Services;

            //builder.Logging.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "LOG", "logger.txt"));
            builder.Logging.ClearProviders();
            builder.Host.UseNLog();

            services.AddScoped<IJwtProvider, JwtProvider>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.TokenValidationParameters = new()
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("gigasecretMardukBlessUs_1435344353687457478283848738382205")) //todo jwtsecret
                    };
                    //options.Events = new JwtBearerEvents
                    //{
                    //    OnMessageReceived = context =>
                    //    {
                    //        context.Token = context.Request.Cookies["asaru"];
                    //        return Task.CompletedTask;
                    //    }
                    //};
                });
            services.AddAuthorization();

            // Add services to the container.
            //services.AddRazorPages();
            services.AddControllers();
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            services.AddServices(builder.Configuration);
            services.AddCors();
            services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            var rmqConfigSection = builder.Configuration.GetSection("RMQSettings");
            builder.Services.Configure<RmqSettings>(rmqConfigSection);
            var rmqSettings = rmqConfigSection.Get<RmqSettings>();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {                    
                    cfg.Host(rmqSettings.Host, rmqSettings.VHost, h =>
                    {
                        h.Username(rmqSettings.Username);
                        h.Password(rmqSettings.Password);
                    });
                });
            });

            services.AddFluentValidationAutoValidation();
            services.AddValidatorsFromAssemblyContaining<UserModelValidator>();

            services.AddGraphQL(SchemaBuilder.New().AddQueryType<Query>().Create());

            var app = builder.Build();
            
            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            else
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
            );

            app.UseAuthentication();
            app.UseAuthorization();

            //app.MapRazorPages();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.MapGraphQL("/graphql");
            app.UsePlayground("/graphql");

            app.Run();
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TaskMapperProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.TaskMappingProfile>();
                cfg.AddProfile<FinalProject.Mapping.ProjectMapperProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.ProjectMappingProfile>();
                cfg.AddProfile<FinalProject.Mapping.UserMapperProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.UserMappingProfile>();
                cfg.AddProfile<FinalProject.Mapping.GroupMappingProfile>();
            });

            return configuration;
        }

        
    }
}