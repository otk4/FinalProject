﻿using AutoMapper;
using FinalProject.Models.ProjectModels;
using Services.Contracts.Project;

namespace FinalProject.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности проекта.
    /// </summary>
    public class ProjectMapperProfile : Profile
    {
        public ProjectMapperProfile()
        {
            CreateMap<ProjectDto, ProjectModel>();
            CreateMap<ProjectModel, ProjectDto>();
        }
    }
}
