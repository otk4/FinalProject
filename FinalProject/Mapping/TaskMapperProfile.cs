﻿using AutoMapper;
using FinalProject.Models.TaskModels;
using Services.Contracts.Task;
using Services.Contracts.TaskHistory;
using Services.Contracts.User;

namespace FinalProject.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности задачи.
    /// </summary>
    public class TaskMapperProfile : Profile
    {
        public TaskMapperProfile()
        {
            CreateMap<TaskDto, TaskModel>()
                .ForMember(d => d.Author, map => map.MapFrom(s => s.Author.Name))
                .ForMember(d => d.Responcible, map => map.MapFrom(s => s.Responcible.Name))
                .ForMember(d => d.ProjectName, map => map.MapFrom(s => s.Project.Name))
                .ForMember(d => d.History, map => map.MapFrom(s => s.History));

            CreateMap<CreatingTaskModel, CreatingTaskDto>();
            CreateMap<UpdatingTaskModel, UpdatingTaskDto>();
            CreateMap<TaskFilterModel, TaskFilterDto>();

            CreateMap<TaskHistoryModel, TaskHistoryDto>();
        }
    }
}
