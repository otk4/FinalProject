﻿using AutoMapper;
using FinalProject.Models;
using Services.Contracts.Group;

namespace FinalProject.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности Группф.
    /// </summary>
    public class GroupMappingProfile : Profile
    {
        public GroupMappingProfile()
        {
            CreateMap<GroupDto, GroupModel>()
                .ReverseMap();
        }
    }
}
