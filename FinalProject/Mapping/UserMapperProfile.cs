﻿using AutoMapper;
using Domain.Entities;
using FinalProject.Models.UserModels;
using Services.Contracts.Task;
using Services.Contracts.User;

namespace FinalProject.Mapping
{
    public class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            CreateMap<UserDto, UserModel>();
            CreateMap<UserModel, UserDto>();

            CreateMap<SimpleUserDto, SImpleUserModel>();

        }
    }
}
