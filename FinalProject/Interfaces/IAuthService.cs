﻿namespace FinalProject.Interfaces
{
    public interface IAuthService
    {
        Task<bool> AuthenticateAsync(string username, string password);
    }
}
