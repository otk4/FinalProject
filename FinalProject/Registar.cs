﻿using FinalProject.Settings;
using Infrastructure.PostgreSqlContext;
using Infrastructure.Repositories.Implementation.Repositories;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Service.Implementations;
using Services.Abstractions;
using Services.Implementations;
using Services.Repositories.Abstractions;

namespace FinalProject
{
    public static class Registar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration.Get<AppSettings>();
            services.AddSingleton(settings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .InstallServices()
                    .ConfigureContext(settings.ConnectionString)
                    .InstallRepositories();
            return services;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
            .AddTransient<ITaskService, TaskService>()
            .AddTransient<IProjectService, ProjectService>()
            .AddTransient<IUserService, UserService>();

            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<ITaskRepository, TaskRepository>()
                .AddTransient<IProjectRepository, ProjectRepository>()
                .AddTransient<ITaskHistoryRepository, TaskHistoryRepository>()
                .AddTransient<IUserRepository, UserRepository>();

            return serviceCollection;
        }

    }
}
