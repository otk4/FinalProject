﻿namespace Domain.Entities
{
    /// <summary>
    /// Группа.
    /// </summary>
    public class Group : BaseEntity, IEntity
    {
        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Пользователи.
        /// </summary>
        public virtual ICollection<UserGroup> Users { get; set; }
    }
}
