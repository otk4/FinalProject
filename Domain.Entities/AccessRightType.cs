﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public enum AccessRightType
    {
        Read = 1,
        Change = 2,
        FullAccess = 3
    }
}
