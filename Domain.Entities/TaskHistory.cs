﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TaskHistory : BaseEntity, IEntity
    {
        public virtual User? User { get; set; }
        public virtual TaskItem Entity { get; set; }
        public HistoryAction Action { get; set; }
        public string Operation { get; set; }
        public DateTime OperationDate { get; set; }
        public string? Comment { get; set; }
    }

    public enum HistoryAction
    {
        Read,
        Create,
        Update,
        Delete
    }
}
