﻿namespace Domain.Entities
{
    /// <summary>
    /// Проект.
    /// </summary>
    public class Project : BaseEntity, IEntity
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Список задач.
        /// </summary>
        public virtual List<TaskItem> Tasks { get; set; }

        /// <summary>
        /// Произнак удаления.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
