﻿namespace Domain.Entities
{
    public interface IEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; }
    }
}
