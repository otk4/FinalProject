﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    /// <summary>
    /// Права.
    /// </summary>
    public class AccessRight : BaseEntity, IEntity
    {
        /// <summary>
        /// Ид объекта прав.
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Тип сущности.
        /// </summary>
        public Guid EntityTypeGuid { get; set; }
    }
}
