﻿namespace Domain.Entities
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User : BaseEntity, IEntity
    {
        /// <summary>
        /// Аватарка юзера
        /// </summary>
        public byte[]? UserPic { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Личный телефон
        /// </summary>
        public string? PrivatePhone { get; set; }

        /// <summary>
        /// Рабочий телефон
        /// </summary>
        public string? WorkPhone { get; set; }

        /// <summary>
        /// Ид чата в Телеграмм
        /// </summary>
        public long ChatId { get; set; }

        /// <summary>
        /// Статус.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Группы.
        /// </summary>
        public virtual ICollection<UserGroup>? Groups { get; set; }
    }
}
