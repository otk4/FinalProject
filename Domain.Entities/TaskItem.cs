﻿using System.Text.Json.Serialization;

namespace Domain.Entities
{
    public enum Priority
    {
        low,
        normal,
        major,
        critical
    }

    /// <summary>
    /// Задача.
    /// </summary>
    public class TaskItem : BaseEntity, IEntity
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст задачи.
        /// </summary>
        public string? ActiveText { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public virtual User? Responcible { get; set; }

        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public virtual User Author { get; set; }

        /// <summary>
        /// Проект.
        /// </summary>
        public virtual Project Project { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// История работы с задачей.
        /// </summary>
        public virtual List<TaskHistory>? History { get; set; }

        /// <summary>
        /// Срок завершения.
        /// </summary>
        public DateTime? DeadLine { get; set; }

        /// <summary>
        /// Признак удаления.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
