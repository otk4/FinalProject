﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    /// <summary>
    /// Права на сущности.
    /// </summary>
    public class AccessRightsEntity : BaseEntity, IEntity
    {
        /// <summary>
        /// Права доступа.
        /// </summary>
        public virtual AccessRight AccessRight { get; set; }

        /// <summary>
        /// Субъект прав.
        /// </summary>
        public virtual User Owner { get; set; }

        /// <summary>
        /// Тип прав.
        /// </summary>
        public AccessRightType AccessRightType { get;set; }
    }
}
