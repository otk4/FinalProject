﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table(nameof(TaskItem))]
    public class TaskItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}