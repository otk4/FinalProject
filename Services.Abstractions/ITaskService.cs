﻿using Services.Contracts.Task;

namespace Services.Abstractions
{
    public interface ITaskService
    {
        /// <summary>
        /// Получить задачу.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО задачи. </returns>
        Task<TaskDto> GetByIdAsync(int id);

        /// <summary>
        /// Создать задачу.
        /// </summary>
        /// <param name="taskDto"> ДТО создаваемой задачи. </param>
        /// <returns> ДТО задачи. </returns>
        Task<TaskDto> CreateAsync(CreatingTaskDto taskDto);

        /// <summary>
        /// Изменить задачу.
        /// </summary>
        /// <param name="id"> Иентификатор. </param>
        /// <param name="taskDto"> ДТО редактируемой задачи. </param>
        Task UpdateAsync(int id, UpdatingTaskDto taskDto);

        /// <summary>
        /// Удалить задачу.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task DeleteAsync(int id);

        /// <summary>
        /// Получить отфильтрованный список задач.
        /// </summary>
        /// <param name="filterDto"> DTO фильтра. </param>
        /// <returns> Список курсов. </returns>
        Task<ICollection<TaskDto>> GetTaskListAsync(TaskFilterDto filterDto);
    }
}
