﻿using Domain.Entities;

namespace Services.Abstractions
{
    public interface IJwtProvider
    {
        string GenerateToken(User user);
    }
}
