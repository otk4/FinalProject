﻿using Services.Contracts.Project;

namespace Services.Abstractions
{
    public interface IProjectService
    {
        /// <summary>
        /// Получить проект.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО проекта. </returns>
        Task<ProjectDto> GetByIdAsync(int id);

        /// <summary>
        /// Создать проект.
        /// </summary>
        /// <param name="taskDto"> ДТО создаваемого проекта. </param>
        Task<int> CreateAsync(CreateProjectDto taskDto);

        /// <summary>
        /// Изменить проект.
        /// </summary>
        /// <param name="id"> Иентификатор. </param>
        /// <param name="taskDto"> ДТО редактируемого проекта. </param>
        Task UpdateAsync(int id, ProjectDto taskDto);

        /// <summary>
        /// Удалить задачу.
        /// </summary>
        /// <param name="id"> Идентификатор.</param>
        Task DeleteAsync(int id);

        /// <summary>
        /// Получить список проектов.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО проекта. </returns>
        Task<ICollection<ProjectDto>> GetAllAsync();
    }
}
