﻿using Services.Contracts.Project;
using Services.Contracts.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface IUserService
    {
        /// <summary>
        /// Получить пользователя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО проекта. </returns>
        Task<UserDto> GetByIdAsync(int id);

        /// <summary>
        /// Создать пользователя.
        /// </summary>
        /// <param name="taskDto"> ДТО создаваемого проекта. </param>
        Task<int> CreateAsync(UserDto userDto);

        /// <summary>
        /// Изменить пользователя.
        /// </summary>
        /// <param name="id"> Иентификатор. </param>
        /// <param name="taskDto"> ДТО редактируемого проекта. </param>
        Task UpdateAsync(int id, UserDto userDto);

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        /// <param name="id"> Идентификатор.</param>
        void Delete(int id);
        
        /// Получить список id и имен юзеров для страницы задач
        /// </summary>
        /// <returns> Список курсов. </returns>
        /// <remarks>todo Доработать под пейджинг/odata/графQL етц</remarks>
        Task<ICollection<SimpleUserDto>> GetAllAsync(CancellationToken token);

        Task<string> Login(string login, string password);
    }
}
