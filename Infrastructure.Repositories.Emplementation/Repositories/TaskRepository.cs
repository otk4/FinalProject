﻿using Domain.Entities;
using Infrastructure.PostgreSqlContext;
using Microsoft.EntityFrameworkCore;
using Services.Contracts.Task;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementation.Repositories
{
    /// <summary>
    /// Репозиторий работы с задачами.
    /// </summary>
    public class TaskRepository : Repository<TaskItem>, ITaskRepository
    {
        public TaskRepository(PostgresqlContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить сущность по ID.
        /// </summary>
        /// <param name="id"> Id сущности. </param>
        /// <param name="cancellationToken"></param>
        /// <returns> Курс. </returns>
        public override async Task<TaskItem> GetAsync(int id, CancellationToken cancellationToken)
        {
            var query = Context.Set<TaskItem>().AsQueryable();
            return await query.SingleOrDefaultAsync(c => c.Id == id);
        }

        /// <summary>
        /// Получить список задач.
        /// </summary>
        /// <param name="filterDto"> DTO фильтра. </param>
        /// <returns> Список задач. </returns>
        public async Task<List<TaskItem>> GetTaskListAsync(TaskFilterDto filterDto)
        {
            var query = GetAll()
                .Where(c => !c.Deleted);

            if (filterDto.ProjectId > 0)
                query = query.Where(c => c.Project.Id == filterDto.ProjectId);

            if (filterDto.UserId > 0)
                query = query.Where(c => c.Responcible.Id == filterDto.UserId);

            return await query.ToListAsync();
        }
    }
}
