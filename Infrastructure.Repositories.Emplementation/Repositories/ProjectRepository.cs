﻿using Domain.Entities;
using Infrastructure.PostgreSqlContext;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementation.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(PostgresqlContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить сущность по ID.
        /// </summary>
        /// <param name="id"> Id сущности. </param>
        /// <param name="cancellationToken"></param>
        /// <returns> Курс. </returns>
        public override async Task<Project> GetAsync(int id, CancellationToken cancellationToken)
        {
            var query = Context.Set<Project>().AsQueryable();
            return await query.SingleOrDefaultAsync(c => c.Id == id);
        }
    }
}
