﻿using Domain.Entities;
using Infrastructure.PostgreSqlContext;
using Microsoft.EntityFrameworkCore;
using Services.Contracts.Task;
using Services.Contracts.User;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Infrastructure.Repositories.Implementation.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(PostgresqlContext context) : base(context)
        {
        }

        public virtual async Task<User> GetActiveUserAsync(int userId)
        {
            var query = await base.GetAsync(userId, CancellationToken.None);

            if (query.Status != Status.Active)
                return null;

            return query;
        }

        public virtual async Task<User> GetByLogin(string login)
        {
            var query = await base.GetAllAsync(CancellationToken.None);
            var user = query.FirstOrDefault(u => u.Login == login);
            if (user == null)
                throw new Exception("Юзер по логину не найден");

            return user;
        }
    }
}
