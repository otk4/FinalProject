﻿using Domain.Entities;
using Infrastructure.PostgreSqlContext;
using Infrastructure.Repositories.Implementation.Repositories;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementation
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(PostgresqlContext context) : base(context)
        {
        }
        public virtual async Task<User> GetActiveUserAsync(int userId)
        {
            var query = await base.GetAsync(userId, CancellationToken.None);

            if (query.Status != Status.Active)
                return null;

            return query;
        }

        public Task<User> GetByLogin(string login)
        {
            throw new NotImplementedException("shiiiiish");
        }
    }
}
