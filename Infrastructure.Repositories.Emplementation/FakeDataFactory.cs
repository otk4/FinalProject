﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementation
{
    public static class FakeDataFactory
    {
        public static IEnumerable<User> Users => new List<User>()
        {
            new() { Name = "Administrator", Status = Status.Active },
            new() { Name = "TestUser", Status = Status.Active },
            new() { Name = "AnotherTestUser", Status = Status.Active },
        };

        public static IEnumerable<Group> Group => new List<Group>()
        {
            new() { Name = "Administrators" },
            new() { Name = "AllUsers" },
        };
        public static IEnumerable<Project> Projects => new List<Project>()
        {
            new() { Name = "First", Text = "Первый проект"},
            new() { Name = "Second", Text = "Второй проект" },
            new() { Name = "Third", Text = "Третий проект" },
        };
        public static IEnumerable<TaskItem> Tasks => new List<TaskItem>()
        {
            new() { Name = "Это раз", ActiveText = "Что то деалть"},
            new() { Name = "Это два", ActiveText = "Делать что то другое" },
            new() { Name = "Это три", ActiveText = "Это когда нибудь потом" },
        };
    }
}
