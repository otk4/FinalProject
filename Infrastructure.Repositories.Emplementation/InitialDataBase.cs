﻿using Infrastructure.PostgreSqlContext;
using Infrastructure.Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlContext
{
    public static class InitialDataBase
    {
        public static void Init(PostgresqlContext context)
        {
            ClearData(context);
            AddData(context);
        }

        private static void ClearData(PostgresqlContext context)
        {
            //context.Database.EnsureDeleted();
            //context.Database.EnsureCreated();
        }

        private static void AddData(PostgresqlContext context)
        {
            context.Users.AddRange(FakeDataFactory.Users);
            context.Groups.AddRange(FakeDataFactory.Group);
            context.Projects.AddRange(FakeDataFactory.Projects);
            context.TaskItems.AddRange(FakeDataFactory.Tasks);
        }
    }
}
