﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetActiveUserAsync(int id);
        Task<User> GetByLogin(string login);
    }
}
