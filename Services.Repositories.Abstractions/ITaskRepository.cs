﻿using Domain.Entities;
using Services.Contracts.Task;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с задачами.
    /// </summary>
    public interface ITaskRepository : IRepository<TaskItem>
    {
        /// <summary>
        /// Получить список задач.
        /// </summary>
        /// <param name="filterDto"> DTO фильтра. </param>
        /// <returns> Список задач. </returns>
        Task<List<TaskItem>> GetTaskListAsync(TaskFilterDto filterDto);
    }
}
