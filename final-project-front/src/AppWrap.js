import { useRoutes } from "react-router-dom";
import LoginPage from "./Components/LoginPage";
import App from "./App";

const AppWrap = () => {
    let routes = useRoutes([
        { path: "*", element: <LoginPage /> },
        { path: "app", element: <App /> },
    ]);
    return routes;
};

export default AppWrap;