import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import AppWrap from './AppWrap.js';
import { BrowserRouter} from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <div className='root'>
    <BrowserRouter>
      <AppWrap />
    </BrowserRouter>
  </div>
);

reportWebVitals();
