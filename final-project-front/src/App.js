import './App.css';
import getUserInfFromCookie from './cookieHelper.js';
import Header from './Components/Header';
import WorkPlace from './Components/WorkPlace';

function App() {

  function getId(){
    let result = getUserInfFromCookie();
    return result.id
  };

  return (
    <>
      <Header userId={getId()} />
      <WorkPlace />
    </>
  );
}

export default App;
