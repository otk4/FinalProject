import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios'
import TaskInfo from './TaskInfo.js';

function Board(props) {

    let trackPlaceStyle = {
        backgroundColor: 'lightgrey',
        width: '50%',
        margin: '10px',
        display: 'flex',
        borderRadius: '10px'
    }
    let trackStyle = {
        backgroundColor: 'rgb(85, 82, 82)',
        height: '100%',
        width: '30%',
        margin: '10px',
        borderRadius: '10px'
    }
    let taskStyle = {
        backgroundColor: 'whitesmoke',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
        cursor: 'grab'
    }
    let taskSignatureStyle = {
        marginTop: '10px',
        paddingLeft: '10px'
    }
    let taskDropdownButtonStyle = {
        borderRadius: '10px',
        borderColor: 'grey',
        borderStyle: 'groove',
        paddingLeft: '10px',
        marginTop: '10px',
        width: '100%',
        backgroundColor: 'whitesmoke'
    }
    let buttonStyle = {
        borderRadius: '5px',
        borderColor: 'grey',
        float: "right"
    }

    const PROJECT_ID = props.projectId;
    const USER_ID = props.userId;

    const stateArr = ['backlog', 'inprogress', 'done'];     //todo можно расширить если будут нужны доптреки
    const [tracks, setTracks] = useState([]);
    const [executors, setExecs] = useState([]);

    const [currentInfo, setInfo] = useState();

    function getBoard(projectId, userId) {

        getExecutorsList();

        let data = JSON.stringify({
            "ProjectId": projectId,
            "UserId": projectId > 0 ? 0 : userId
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/task/projectTaskList',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };
        axios.request(config).then((resp) => {
            let result = stateArr.map((item) => { return { id: item, tasks:[]}});
            if(resp.data.length > 0){
                resp.data
                .map((item) =>  {
                    let tr = result.find(e => e.id == item.state);
                    if(tr){
                        tr.tasks.push(item);
                    }else{
                        result.push({id: item.state, tasks: [ item ]})
                    }
                });
            }
            setTracks(result)
        }).catch((err) => {
            console.error(err);
        });

       
    };

    useEffect(() => {
        getBoard(props.projectId, props.userId);
      }, []);
    const [currentTrack, setCurrentTrack] = useState(null);
    const [currentTask, setCurrentTask] = useState(null);

    function dragStartHandler(e, track, task) {
        setCurrentTrack(track);
        setCurrentTask(task);
    }
    function dragLeaveHandler(e) {
        e.target.style.boxShadow = 'none';
    }
    function dragEndHandler(e) {
        e.target.style.boxShadow = 'none';
    }
    function dragOverHandler(e) {
        e.preventDefault();
        if (e.target.className == 'taskItem') {
            e.target.style.boxShadow = '0 4px 3px blue';
        }
    }
    function dragDropHandler(e, track, task) {
        e.preventDefault();

        const curIndex = currentTrack.tasks.indexOf(currentTask);
        currentTrack.tasks.splice(curIndex, 1);

        const dropIndex = track.tasks.indexOf(task);
        track.tasks.splice(dropIndex + 1, 0, currentTask);

        setTracks(tracks.map(t => {
            if (t.id == track.id) {
                return track;
            }
            if (t.id == currentTrack.id) {
                return currentTrack;
            }
            return t;
        }));

        let newTask = JSON.parse(JSON.stringify(currentTask));
        newTask.state = track.id
        updateTaskQuery(newTask);
    }

    async function getExecutorsList() {

        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/user/',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
        };
        await axios.request(config).then((resp) => {
            setExecs(resp.data);
        }).catch((err) => {
            console.error(err);
        });
    }
    function showAddPanel(e) {
        e.target.parentNode.parentNode.getElementsByClassName('addTask')[0].style.display = 'block';
    }
    function hideAddPanel(e) {
        e.target.parentNode.style.display = 'none';
    }
    function createTaskQuary(task) {
        let data = JSON.stringify({
            "name": task.taskName,
            "activeText": task.taskActiveText,
            "priority": task.taskPriority,
            "responcibleId": task.execId,
            "authorId" : USER_ID,
            "projectId": PROJECT_ID, //создание задачи будет только из под проекта
            "state" : task.taskState
        });

        console.warn(data);

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/task/',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };

        axios.request(config).then((resp) => {
            var result = resp.data;
            console.error(result);
            setTracks(tracks.map((track) => {
                if (track.id == result.state) {
                    track.tasks.splice(0, 0, result);
                    return track;
                }
                return track;
            }));
        }).catch((err) => {
            console.error(err);
            console.error(err.response?.data);
        });
    }

    function getTaskObjFromElement(elem){
        let selects = elem.getElementsByTagName('select');
        let state = elem.parentNode.id;
        let newTask = {
            taskId: elem.getElementsByClassName('taskId')[0]?.innerHTML, 
            taskName: elem.getElementsByTagName('input')[0].value,
            taskActiveText: "", //todo 
            taskPriority: selects[0].value,
            taskState: state,
            execId: selects[1].value,
            taskProjId: elem.getElementsByClassName('taskProjId')[0]?.getAttribute('name'),
            taskProjName: elem.getElementsByClassName('taskProjId')[0]?.innerHTML
        };
        return newTask;
    }

    function addTask(e) {
        let elem = e.target.parentNode;
        let newTask = getTaskObjFromElement(elem);
        console.warn(newTask);
        createTaskQuary(newTask);
        hideAddPanel(e);
    }
    function saveChangesTask(e, id, isResp) {
        let val = e.target.value;
        console.debug("saveChangesTask val:" + val);
        console.debug("saveChangesTask isResp:" + isResp);
        let task = tracks.flatMap(tr => tr.tasks).find(t => t.id === id);
        if(task != null)
        {
            let newTask = JSON.parse(JSON.stringify(task));
            if(isResp)
                newTask.responcible = new {id: val};
            else
                newTask.priority = val;
        
            updateTaskQuery(newTask);
        }
        
    }
    function updateTaskQuery(task){
        
        console.warn(task)

        let data = JSON.stringify({
            "name": task.name,
            "activeText": task.activeText,
            "priority": task.priority,
            "responcibleId": task.responcible.id,
            "authorId" : USER_ID,
            "projectId": task.project.id,
            "deadline": task.deadline ?? null,
            "state" : task.state,
        });

        console.warn(data)

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/task/' + task.id,
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };

        axios.request(config).then((resp) => {
            setTracks(tracks.map((track) => {
                if (track.id == task.state) {
                    let indx = track.tasks.findIndex(t => t.id == task.id);
                    track.tasks.splice(indx, 1, task);
                    return track;
                }
                return track;
            }));
            getInfo(task.id);
        }).catch((err) => {
            console.error(err);
            console.error(err.response.data);
        });
    }

    const update = (p) => {
        setTracks(tracks.map((track) => {
            if (track.id == p.state) {
                let index = track.tasks.findIndex(t => t.id == p.id);
                if(index > -1)
                    track.tasks.splice(index, 1, p);

                return track;
            }
            return track;
        }));
    }

    function getInfo(id){
        let inf = tracks.flatMap(tr => tr.tasks).find(t => t.id === id);
        if(inf != null)
            setInfo(<TaskInfo updateList={update} taskInfoObj={inf} executors={executors} userId={USER_ID}/>);
    }

    return (
        <div style={trackPlaceStyle}>
            {
                tracks.map(track =>
                    <div style={trackStyle} id={track.id}>
                        <div
                            onDragStart={(e) => dragStartHandler(e, track, null)}
                            onDragEnd={(e) => dragEndHandler(e)}
                            onDragOver={(e) => dragOverHandler(e)}
                            onDragLeave={(e) => dragLeaveHandler(e)}
                            onDrop={(e) => dragDropHandler(e, track, null)}
                            style={taskStyle} className="taskItem">
                            {track.id}
                            <button style={props.projectId == 0 ? { display: 'none' } : buttonStyle} onClick={(e) => showAddPanel(e)}>+</button>
                        </div>
                        <div style={taskStyle} className='addTask'
                        >
                            <input style={taskSignatureStyle}></input>
                            <select style={taskDropdownButtonStyle}>
                                <option value={0} >low</option>
                                <option value={1}>normal</option>
                                <option value={2}>major</option>
                                <option value={3}>critical</option>
                            </select>
                            <select style={taskDropdownButtonStyle}>
                                {executors.map((item) => <option value={item.id}>{item.name}</option>)}
                            </select>
                            <button style={buttonStyle} onClick={(e) => hideAddPanel(e)}>Отмена</button>
                            <button style={buttonStyle} onClick={(e) => addTask(e)}>Добавить</button>
                        </div>

                        {
                            track.tasks.map(task =>
                                <div style={taskStyle}
                                    draggable={true}
                                    onDragStart={(e) => dragStartHandler(e, track, task)}
                                    onDragLeave={(e) => dragLeaveHandler(e)}
                                    onDragEnd={(e) => dragEndHandler(e)}
                                    onDragOver={(e) => dragOverHandler(e)}
                                    onDrop={(e) => dragDropHandler(e, track, task)}
                                    className="taskItem"
                                    onClick = {() => getInfo(task.id)}
                                >
                                    <span style={taskSignatureStyle} className='taskId'>{task.id}</span>
                                    <span style={taskSignatureStyle} className='taskName'>{task.name}</span>
                                    <select style={taskDropdownButtonStyle} onChange={(e) => saveChangesTask(e, task.id)} >
                                        <option value={0} selected={task.priority == 0}>low</option>
                                        <option value={1} selected={task.priority == 1}>normal</option>
                                        <option value={2} selected={task.priority == 2}>major</option>
                                        <option value={3} selected={task.priority == 3}>critical</option>
                                    </select>
                                    <select style={taskDropdownButtonStyle} onChange={(e) => saveChangesTask(e, task.id, true)}>
                                        {executors.map((item) => <option value={item.id} selected={task.responcible.id == item.id}>{item.name}</option>)}
                                    </select>
                                    <span style={props.projectId == 0 ? taskSignatureStyle : {display:"none"}} className="taskProjId" name={task.project?.id}><br/>Проект: {task.project?.name}</span>
                                </div>
                            )
                        }
                    </div>
                )}
            {currentInfo}
        </div>
    );
}

export default Board;