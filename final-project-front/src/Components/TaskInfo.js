import { useState } from "react";
import { useEffect } from 'react';
import axios from 'axios'

function TaskInfo({ updateList, taskInfoObj, executors, userId }) {

    let projInfoStyle = {
        backgroundColor: 'whitesmoke',
        width: '40%',
        marginLeft: '10px',
        display: 'block'
    };
    let taskStyle = {
        backgroundColor: 'white',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
    }
    let taskDropdownButtonStyle = {
        borderRadius: '10px',
        borderColor: 'grey',
        borderStyle: 'groove',
        paddingLeft: '10px',
        marginTop: '10px',
        marginBottom: '10px',
        width: '100%',
        backgroundColor: 'white',
        fontFamily: 'Consolas',
    }

    const [info, setInfo] = useState(taskInfoObj);
    const USER_ID = userId;

    useEffect(() => {
        setInfo(taskInfoObj);
    }, [taskInfoObj]);
    function onChangeElement(e, type) {
        let val = e.target.value;
        let newInfo = JSON.parse(JSON.stringify(info));

        switch (type) {
            case "name":
                newInfo.name = val;
                break;
            case "activeText":
                newInfo.activeText = val;
                break;
            case "priority":
                newInfo.priority = val;
                break;
            case "responcible":
                newInfo.responcible = { id: val };
                break;
            case "deadLine":
                newInfo.deadLine = val;
                break;
            default:
                alert("Неизвестный тип элемента");
        }
        setInfo(newInfo);
    }

    function saveChangesProject(e) {
        let data = JSON.stringify({
            "name": info.name,
            "activeText": info.activeText,
            "priority": info.priority,
            "responcibleId": info.responcible.id,
            "authorId": USER_ID,
            "projectId": info.project.id,
            "deadLine": info.deadLine,
            "state": info.state
        });

        console.warn(data)

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/task/' + info.id,
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };

        axios.request(config).then((resp) => {
            getTask(info.id);
        }).catch((err) => {
            console.error(err);
            console.error(err.response?.data);
        });
    }

    function getTask(id){
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/task/' + id,
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            }
        };

        axios.request(config).then((resp) => {
            setInfo(resp.data);
            updateList(resp.data);
        }).catch((err) => {
            console.error(err);
            console.error(err.response?.data);
        });
    }

    return (
        <div id={info.id} style={projInfoStyle}>
            <input style={{ marginTop: "20px", marginLeft: "10px" }} value={info.name} onChange={(e) => onChangeElement(e, "name")}></input>
            <textarea value={info.activeText} style={taskStyle} onChange={(e) => onChangeElement(e, "activeText")}></textarea>
            <select style={taskDropdownButtonStyle} onChange={(e) => onChangeElement(e, "priority")}>
                <option value={0} selected={info.priority == 0}>low</option>
                <option value={1} selected={info.priority == 1}>normal</option>
                <option value={2} selected={info.priority == 2}>major</option>
                <option value={3} selected={info.priority == 3}>critical</option>
            </select>
            <select style={taskDropdownButtonStyle} onChange={(e) => onChangeElement(e, "responcible")}>
                {executors.map((item) => <option value={item.id} selected={info.responcible.id == item.id}>{item.name}</option>)}
            </select>
            <div style={(info.deadLine != null ? { display: "block" } : { display: "none" })}>
                <span style={{ marginLeft: "10px", fontFamily: 'Consolas', }}>Дедлайн:</span>
                <input style={{ marginLeft: "10px", fontFamily: 'Consolas' }} type="date" value={new Date(info.deadLine).toISOString().split('T')[0]} onChange={(e) => onChangeElement(e, "deadLine")}></input>
            </div>
            <ul>
                {info.history.map((item) => <li style={{ fontFamily: 'Consolas', }}>{new Date(item.operationDate).toString()} {item.comment} ({item.user?.name})</li>)}
            </ul>
            <button style={taskStyle} onClick={(e) => saveChangesProject(e)}>Сохранить изменения</button>
        </div>
    );
}

export default TaskInfo;