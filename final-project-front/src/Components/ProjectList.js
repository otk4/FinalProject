import { useState } from "react";
import { useEffect } from 'react';
import axios from 'axios';
import ProjectInfoBoard from './ProjectInfoBoard';


function ProjectList(props) {

    let style = {
        backgroundColor: 'lightgrey',
        width: '30%' 
    };
    
    let taskStyle = {
        backgroundColor: 'whitesmoke',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
        width: "90%"
    }
    const [projects, setProjects] = useState([]);
    const [currentInfo, setInfo] = useState();
    
    useEffect(() => {
        getProjectList();
      }, []
    );

    const update = (p) => {
        let index = projects.findIndex(x => x.id === p.id);
        if(index === -1)
            projects.push(p);
        else
            projects[index] = p;
        setProjects(projects.map((p) => {return p;}));
    }

    function getProjectList(userId){
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/project',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
        };
        axios.request(config).then((resp) => {
            if(resp.data.length > 0){
                setProjects(resp.data);
            }else{
                let emptyProj = {
                    id: 0,
                    name: "",
                    text: ""
                }
                setInfo(<ProjectInfoBoard projectInfo={emptyProj} updateList={update}/>);
            }
        }).catch((err) => {
            console.error(err);
        });
    }

    function getProjInfo(id){
        console.debug("getProjInfo " + id);
        let proj = projects.find((p) => p.id == id);
        if(proj != null)
            setInfo(<ProjectInfoBoard updateList={update} projectInfo={proj} totasks={props.setPlace}/>);
    }

    return (
        <>
            <div style={style}>
                <ul>
                    {projects.map(proj => <li><button className='focus' style={taskStyle} onClick = {() => getProjInfo(proj.id)}>{proj.name}</button></li>)}
                </ul>
            </div>
            {currentInfo}
        </>
    );
}

export default ProjectList;