import UserPreview from './UserPreview';
import { useNavigate } from "react-router-dom";

function Header({userId}) {
  let style = {
      backgroundColor: 'blueviolet',
      height: '5%',
      display: 'flex'
  };

  const navigate = useNavigate();

  function logOut(){
    alert("logOut");
    document.cookie.split(';').forEach(cookie => {
      const eqPos = cookie.indexOf('=');
      const name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    });
    navigate("*");
  }

  return (
    <header style={style}>
      <UserPreview userIdp={userId}/>
      <button onClick={() => logOut()} style={{color: 'lightGrey', fontFamily: 'Consolas', float: "right", borderRadius: '5px', backgroundColor: "rgb(177 101 249)"}}>Выйти</button>      
    </header>
  );
}

export default Header;