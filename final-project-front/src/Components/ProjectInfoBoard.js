import axios from 'axios'
import { useState } from "react";
import { useEffect } from 'react';
import Board from './Board';
import getUserInfFromCookie from '../cookieHelper.js';

function ProjectInfoBoard({ updateList, projectInfo, totasks}){
    let projInfoStyle = {
        backgroundColor: 'whitesmoke',
        width: '30%',
        marginLeft: '10px',
        display: 'block'
    };

    let taskStyle = {
        backgroundColor: 'whitesmoke',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
    }

    let invicibleStyle = {
        display: "none"
    }

    const [project, setProj] = useState(projectInfo);

    useEffect(() => {
        console.debug("useEffect");
        setProj(projectInfo);
      }, [projectInfo]);

    function onInputChangeName(e){
        var elem = e.target;
        let newP = {
            id: project.id,
            name: elem.value,
            text: project.text
        };
        setProj(newP);
    }
    function onInputChangeText(e){
        var elem = e.target;
        let newP = {
            id: project.id,
            name: project.name,
            text: elem.value
        };
        setProj(newP);
    }
    
    function createProject(e){

        let data = JSON.stringify({
            "name": project.name,
            "text": project.text
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/project/',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };

        axios.request(config).then((resp) => {
           console.log(resp.data);
           if(resp.data > 0){
                let newProj = {
                    id: resp.data,
                    name: project.name,
                    text: project.text
                }
                setProj(newProj);
                updateList(newProj);
           }
        }).catch((err) => {
            console.error(err);
        });
    }

    function toProj(){
        console.error(project);
        let result = getUserInfFromCookie();
        totasks(<Board projectId={project.id} userId={result.id}/>); //todo current user id
    }
    function saveChangesProject(){
        console.error(project);

        let data = JSON.stringify({
            "id": project.id,
            "name": project.name,
            "text": project.text,
            "tasks": []
        });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/project/' + project.id,
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data : data
        };

        axios.request(config).then((resp) => {
           console.log(resp.data);
           updateList(project);
        }).catch((err) => {
            console.error(err);
        });
    }

    return (
            <div id={projectInfo.id} style={projInfoStyle}>
                <input style={{marginLeft: "10px"}} value={project.name} onChange={(e) => onInputChangeName(e)}></input>
                <textarea value={project.text} style={taskStyle} onChange={(e) => onInputChangeText(e)}></textarea>
                <button style={project.id > 0 ? taskStyle : invicibleStyle} onClick={() => toProj()}>К задачам</button>
                <button style={taskStyle} onClick={(e) => createProject(e)}>Создать {project.id > 0 ? "новый": ""}</button>
                <button style={project.id > 0 ? taskStyle : invicibleStyle} onClick={(e) => saveChangesProject(e)}>Сохранить изменения</button>
            </div>
    );
}

export default ProjectInfoBoard;