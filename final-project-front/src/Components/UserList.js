import { useState } from "react";
import { useEffect } from 'react';
import axios from 'axios';
import UserInfo from './UserInfo';

function ProjectList(props) {

    let style = {
        backgroundColor: 'lightgrey',
        width: '30%' 
    };
    
    let taskStyle = {
        backgroundColor: 'whitesmoke',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
        width: "90%"
    }

    const [users, setUserList] = useState([]);
    const [currentInfo, setInfo] = useState();

    useEffect(() => {
        getUserList();
      }, []
    );

    function getUserList(){
        /* let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/user',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
        };*/

        let q = `{
            users{
        id,
        name
        }
    }    `;

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/graphql',
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + document.cookie.replace('asaru=', '')
            },
            data: {
                query: q
            }
        };

        axios.request(config).then((resp) => {
            console.error(resp.data.data.users);
            if(resp.data.data.users.length > 0){
                setUserList(resp.data.data.users);
            }
        }).catch((err) => {
            console.error(err);
        });
    }

    function getUserInfo(id){
        console.debug("getUserInfo " + id);
        if(id === 0)
            setInfo(null);    
        setInfo(<UserInfo userIdProp={id} updateList={update}  /*totasks={props.setPlace} *//>);
    }

    const update = (p) => {
        let index = users.findIndex(x => x.id === p.id);
        if(index === -1)
            users.push(p);
        else
        users[index] = p;
        setUserList(users.map((p) => {return p;}));
    }

    function test(){
        console.warn(users);
    }

    return (
        <>
            <div style={style}>
                <ul>
                    {users.map(userItem => <li><button className='focus' style={taskStyle} onClick = {() => getUserInfo(userItem.id)}>{userItem.name}</button></li>)}
                </ul>
                <button  style={taskStyle} onClick = {() => getUserInfo(0)}>Создать</button>
                {/* <button  style={taskStyle} onClick = {() => test()}>TEST</button> */}
            </div>
            {currentInfo}
        </>
    );
}

export default ProjectList;