import { useState } from "react";
import axios from 'axios';
import { useNavigate } from "react-router-dom";


function LoginPage() {

    let style = {
        backgroundColor: 'whitesmoke',
        width: '300px',
        marginLeft: "45%",
        borderRadius: '20px',
        marginTop: "50px",

    };
    let taskStyle = {
        backgroundColor: 'whitesmoke',
        margin: '10px',
        fontFamily: 'Consolas',
        padding: '10px',
        color: 'grey',
        borderRadius: '10px',
        borderStyle: "groove",
        marginLeft: "20px",
        width: "85%"
    };
    let labelStyle = {
        fontFamily: 'Consolas',
        width: '50%',
        marginLeft: "25px",
        display: 'flex'
    };

    const [authData, setData] = useState(
        {
            log: null,
            pass: null
        }
    );
    const navigate = useNavigate();

    function change(e, type) {
        let val = e.target.value;
        switch (type) {
            case "login":
                authData.log = val;
                break;
            case "pass":
                authData.pass = val;
                break;
        }
        setData(JSON.parse(JSON.stringify(authData)));
    }

    function login() {
        if (authData === null || authData === undefined) {
            alert("Ошибка объект данных пуст");
            return;
        }

        if (authData.log === null || authData.log.trim().length <= 0) {
            alert("Логин должен быть заполнен");
            return;
        }

        if (authData.pass === null || authData.pass.trim().length <= 0) {
            alert("Пароль должен быть заполнен");
            return;
        }
        console.warn(authData);

        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.REACT_APP_BACK_URL + '/user/login?login='+ authData.log+'&pass=' + authData.pass,
            headers: { 
              'Content-Type': 'application/json'
            },
        };
        axios.request(config).then((resp) => {
            //alert("succes");
            document.cookie = 'asaru=' + resp.data + ';max-age=604800;'
            navigate("/app");
        }).catch((err) => {
            alert("Не удалось выполнить вход! Проверьте введенные данные и повторите попытку");
            console.error(err);
        });
    };


    return (
        <div style={style}>
            <div>
                <label style={labelStyle}>Логин</label>
                <input style={{ fontFamily: 'Consolas', marginLeft: "25px" }} onChange={(e) => change(e, "login")} />
            </div>
            <div>
                <label style={labelStyle}>Пароль</label>
                <input style={{ fontFamily: 'Consolas', marginLeft: "25px" }} onChange={(e) => change(e, "pass")} />
            </div>
            <button style={taskStyle} onClick={() => login()}>Вход</button>
        </div>
    );
}
export default LoginPage;