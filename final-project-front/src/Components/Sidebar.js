import Board from './Board';
import ProjectList from './ProjectList';
import UserInfo from './UserInfo';
import UserList from './UserList';
import { useState } from "react";
import { useEffect } from 'react';
import getUserInfFromCookie from '../cookieHelper.js';

function Sidebar(props) {
    let style = {
        backgroundColor: 'whitesmoke',
        height: '100%',
        width: '10%'
    };

    let buttonStyle = {
        borderRadius: '5px',
        borderColor: 'grey',
        display: 'flex',
        margin: '10px',
        width: '90%'
    }

    function hasRigths(){
        var result = getUserInfFromCookie();
        console.warn(result);
        if(result?.rights?.includes("Administrators"))
            return true;
        else
            return false;
    };

    function getId(){
        var result = getUserInfFromCookie();
        console.warn(result);
        return result.id;
    }
    

    function getPlace(e, projId, userId){
        switch(e.target.name){
            case 'userInfo':
                return props.handlePlace(<UserInfo userIdProp={userId}/>);
            case 'myboard':
                return props.handlePlace(<Board projectId={0} userId={userId}/>);
            case 'projectList':
                return props.handlePlace(<ProjectList setPlace={props.handlePlace}/>);
            case 'userList':
                return props.handlePlace(<UserList setPlace={props.handlePlace}/>);
            default:
                return <></>;
        }
    }

    return (
        <div style={style}>
            <div>
                <button className="focus" style={buttonStyle} name='userInfo' onClick={(e) => {getPlace(e, 0, getId())}}>Мой аккаунт</button>
                <button className='focus' style={buttonStyle} name='myboard' onClick={(e) => {getPlace(e, 0, getId())}}>Мои задачи</button>
                <button className='focus' style={buttonStyle} name='projectList' onClick={getPlace}>Проекты</button>
                <button className='focus' style={hasRigths() ? buttonStyle : {display: "none"}} name='userList' onClick={getPlace}>Пользователи(админ)</button>
            </div>
        </div>
    );
}

export default Sidebar;