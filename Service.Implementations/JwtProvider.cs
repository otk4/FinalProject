﻿using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using Services.Abstractions;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services.Implementations
{
    public class JwtProvider : IJwtProvider
    {
        public string GenerateToken(User user)
        {
            var secretKey = "gigasecretMardukBlessUs_1435344353687457478283848738382205";//todo спрятать получше но пока так jwtSecret

            var credentials = new SigningCredentials
            (
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)),
                SecurityAlgorithms.HmacSha256
            );

            Claim[] claims = [new("id", user.Id.ToString()), new("rights", string.Join(";", user.Groups.Select(g => g.Group.Name)))];

            var token = new JwtSecurityToken
            (
                claims: claims,
                signingCredentials: credentials,
                expires: DateTime.UtcNow.AddHours(12) //todo в параметры
            );

            return new JwtSecurityTokenHandler().WriteToken (token);
        }
    }
}
