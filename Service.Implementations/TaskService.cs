﻿using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Contracts.Task;
using Services.Repositories.Abstractions;
using MassTransit;

namespace Service.Implementations
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskHistoryRepository _taskHistoryRepository;
        private readonly IBus _bus;

        private readonly ILogger<TaskService> _logger;

        public TaskService(IMapper mapper, 
                            ITaskRepository taskRepository, 
                            IUserRepository userRepository, 
                            IProjectRepository projectRepository,
                            ILogger<TaskService> logger,
                            ITaskHistoryRepository taskHistoryRepository,
                            IBus bus)
        {
            _mapper = mapper;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _taskHistoryRepository = taskHistoryRepository;
            _logger = logger;
            _bus = bus;
        }

        /// <summary>
        /// Получить задачу.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО задачи. </returns>
        public async Task<TaskDto> GetByIdAsync(int id)
        {
            var taskItem = await _taskRepository.GetAsync(id, CancellationToken.None);
            return _mapper.Map<TaskItem, TaskDto>(taskItem);
        }

        /// <summary>
        /// Создать задачу.
        /// </summary>
        /// <param name="creatingTaskDto"> ДТО создаваемой задачи. </param>
        /// <returns> Созданная задача</returns>
        /// <remarks>Изначально было возвращение id, но чтобы не дергать апи на гет и получать историю лучше возвращать весь объект</remarks>
        public async Task<TaskDto> CreateAsync(CreatingTaskDto creatingTaskDto)
        {
            var task = _mapper.Map<CreatingTaskDto, TaskItem>(creatingTaskDto);
            if (creatingTaskDto.AuthorId > 0)
                task.Author = await _userRepository.GetAsync(creatingTaskDto.AuthorId, CancellationToken.None);
            if (creatingTaskDto.ResponcibleId > 0)
                task.Responcible = await _userRepository.GetAsync(creatingTaskDto.ResponcibleId, CancellationToken.None);
            if (creatingTaskDto.ProjectId > 0)
                task.Project = await _projectRepository.GetAsync(creatingTaskDto.ProjectId, CancellationToken.None);

            task.Priority = (Priority)creatingTaskDto.Priority;
            task.State = creatingTaskDto.State;
            task.History = new List<TaskHistory>
            {
                new TaskHistory()
                {
                    User = task.Author,
                    Action = HistoryAction.Create,
                    Entity = task,
                    Operation = "Создание задачи",
                    Comment = $"Создание задачи пользователем ИД {creatingTaskDto.AuthorId}",
                    OperationDate = DateTime.UtcNow
                },
            };

            var createdTask = await _taskRepository.AddAsync(task);
            await _taskRepository.SaveChangesAsync();

            await SendToRabbitMQ(task.Responcible, $"Task ID={task.Id} created: {task.ActiveText}");

            return _mapper.Map<TaskItem, TaskDto>(createdTask);
        }

        /// <summary>
        /// Изменить курс.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <param name="updatingTaskDto"> ДТО редактируемой задачи. </param>
        public async Task UpdateAsync(int id, UpdatingTaskDto updatingTaskDto)
        {
            var task = await _taskRepository.GetAsync(id, CancellationToken.None);
            if (task == null)
                throw new Exception($"Задача с идентфикатором {id} не найдена");

            _logger.LogDebug("UpdatingTaskDto in TaskService:" + System.Text.Json.JsonSerializer.Serialize(updatingTaskDto));


            var history = new TaskHistory() { Action = HistoryAction.Update };

            history.Operation = "Редактирование задачи";
            history.OperationDate = DateTime.UtcNow;

            if (updatingTaskDto.AuthorId > 0)
                history.User = await _userRepository.GetAsync(updatingTaskDto.AuthorId, CancellationToken.None);

            if (!string.Equals(task.Name, updatingTaskDto.Name))
                history.Comment = $"Редактирование наименования с {task.Name} на {updatingTaskDto.Name}";

            if (!string.Equals(task.ActiveText, updatingTaskDto.ActiveText)) //ещвщ
                history.Comment += $"\r\nРедактирование текста задачи с {task.ActiveText} на {updatingTaskDto.ActiveText}";

            if ((int)task.Priority != updatingTaskDto.Priority) //ещвщ
                history.Comment += $"\r\nСмена приоритета {updatingTaskDto.Priority.ToString()}";
            
            if (task.Responcible.Id != updatingTaskDto.ResponcibleId)
                history.Comment += string.Format("\r\nСмена ответсвенного {0}", updatingTaskDto.ResponcibleId);

            if (task.Project.Id != updatingTaskDto.ProjectId)
                history.Comment += string.Format("\r\nСмена проекта на {0}", updatingTaskDto.ProjectId);

            if (!string.Equals(task.State, updatingTaskDto.State))  //todo
                history.Comment += string.Format("\r\nСмена статуса на {0}", updatingTaskDto.State);

            task.Name = updatingTaskDto.Name;
            task.ActiveText = updatingTaskDto.ActiveText;
            task.Priority = (Priority)updatingTaskDto.Priority;
            
            if(!string.IsNullOrEmpty(history.Comment))
                history.Entity = task;

            var responcible = await _userRepository.GetActiveUserAsync(updatingTaskDto.ResponcibleId);
            task.Responcible = responcible;
            task.State = updatingTaskDto.State;

            if (!string.IsNullOrEmpty(history.Comment))
            {
                history.Comment = history.Comment.Trim();
                _taskHistoryRepository.AddAsync(history);
            }

            _taskRepository.Update(task);
            await _taskRepository.SaveChangesAsync();

            await SendToRabbitMQ(task.Responcible, $"Task ID={task.Id} updated: {task.ActiveText}");
        }

        /// <summary>
        /// Удалить задачу.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        public async Task DeleteAsync(int id)
        {
            var task = await _taskRepository.GetAsync(id, CancellationToken.None);
            task.Deleted = true;

            await SendToRabbitMQ(task?.Responcible, $"Task ID={task.Id} deleted");

            await _taskRepository.SaveChangesAsync();

            
        }

        /// <summary>
        /// Получить список задач по проекту.
        /// </summary>
        /// <param name="TaskFilterDto"> DTO Фильтра. </param>
        /// <returns> Список задач. </returns>
        public async Task<ICollection<TaskDto>> GetTaskListAsync(TaskFilterDto filterDto)
        {
            ICollection<TaskItem> entities = await _taskRepository.GetTaskListAsync(filterDto);
            return _mapper.Map<ICollection<TaskItem>, ICollection<TaskDto>>(entities);
        }

        /// <summary>
        /// Отправка сообщения через RabbitMQ
        /// </summary>
        /// <param name="recipient">Получатель</param>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public async Task SendToRabbitMQ(User recipient, string message)
        {
            if (recipient != null && recipient.ChatId != 0)
            {
                var rmqmessage = new TelegramEvent { ChatId = recipient.ChatId, Message = message };
                var endpoint = await _bus.GetSendEndpoint(new Uri("queue:telegram_event"));
                await endpoint.Send(rmqmessage);
            }
        }
    }
}
