﻿using AutoMapper;
using Domain.Entities;
using Services.Abstractions;
using Services.Contracts.User;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IJwtProvider _jwtProvider;

        public UserService(IMapper mapper, IUserRepository userRepository, IJwtProvider jwtProvider)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _jwtProvider = jwtProvider;
        }

        public async Task<int> CreateAsync(UserDto userDto)
        {
            var user = _mapper.Map<UserDto, User>(userDto);
            var createdUser = await _userRepository.AddAsync(user);
            await _userRepository.SaveChangesAsync();

            return createdUser.Id;
        }

        public void Delete(int id)
        {
            _userRepository.Delete(id);
            _userRepository.SaveChangesAsync();
        }

        public async Task<UserDto> GetByIdAsync(int id)
        {
            var user = await _userRepository.GetAsync(id, CancellationToken.None);
            return _mapper.Map<User, UserDto>(user);
        }

        public async Task UpdateAsync(int id, UserDto userDto)
        {
            var user = await _userRepository.GetAsync(id, CancellationToken.None);
            if (user == null)
            {
                throw new Exception($"Пользователь с идентфикатором {id} не найден");
            }

            user.Name = userDto.Name;
            user.Login = userDto.Login;
            user.Email = userDto.Email;
            user.Password = userDto.Password;

            user.UserPic = Convert.FromBase64String(userDto.UserPic);
            user.WorkPhone = userDto.WorkPhone;
            user.PrivatePhone = userDto.PrivatePhone;
            user.ChatId = userDto.ChatId;

            _userRepository.Update(user);
            await _userRepository.SaveChangesAsync();
        }

        public async Task<ICollection<SimpleUserDto>> GetAllAsync(CancellationToken token)
        {
            var allUsers = await _userRepository.GetAllAsync(token);
            if (allUsers == null)
                throw new Exception("Список юзеров был null");

            return allUsers.Select(u => _mapper.Map<SimpleUserDto>(u)).ToList();
        }

        #region auth

        public async Task<string> Login(string login, string password)
        {
            var user = await _userRepository.GetByLogin(login);
            if (user.Password != password) //todo вообще надо бы хешировать
                throw new Exception("Некорректный логин или пароль");

            var token = _jwtProvider.GenerateToken(user);

            return token;
        }

        #endregion auth
    }
}
