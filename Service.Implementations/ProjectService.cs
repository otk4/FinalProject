﻿using AutoMapper;
using Domain.Entities;
using Services.Abstractions;
using Services.Contracts.Project;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IMapper mapper, IProjectRepository projectRepository)
        {
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        /// <summary>
        /// Получить проект.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО проекта. </returns>
        public async Task<ProjectDto> GetByIdAsync(int id)
        {
            var project = await _projectRepository.GetAsync(id, CancellationToken.None);
            return _mapper.Map<Project, ProjectDto>(project);
        }

        /// <summary>
        /// Создать проект.
        /// </summary>
        /// <param name="creatingTaskDto"> ДТО создаваемого проекта. </param>
        /// <returns> Идентификатор. </returns>
        public async Task<int> CreateAsync(CreateProjectDto creatingProjectDto)
        {
            var project = _mapper.Map<CreateProjectDto, Project>(creatingProjectDto);
            var createdTask = await _projectRepository.AddAsync(project);
            await _projectRepository.SaveChangesAsync();

            return createdTask.Id;
        }

        /// <summary>
        /// Изменить проект.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <param name="updatingTaskDto"> ДТО редактируемого проекта. </param>
        public async Task UpdateAsync(int id, ProjectDto updatingProjectDto)
        {
            var project = await _projectRepository.GetAsync(id, CancellationToken.None);
            if (project == null)
            {
                throw new Exception($"Проект с идентфикатором {id} не найден");
            }

            project.Name = updatingProjectDto.Name;
            project.Text = updatingProjectDto.Text;

            _projectRepository.Update(project);
            await _projectRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить проект.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        public async Task DeleteAsync(int id)
        {
            var project = await _projectRepository.GetAsync(id, CancellationToken.None);
            project.Deleted = true;
            await _projectRepository.SaveChangesAsync();
        }

        public async Task<ICollection<ProjectDto>> GetAllAsync()
        {
            var projects = await _projectRepository.GetAllAsync(CancellationToken.None);
            return _mapper.Map<List<Project>, ICollection<ProjectDto>>(projects);
        }
    }
}
