﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts.Task;
using Services.Contracts.TaskHistory;

namespace Services.Implementations.Mapping
{
    public class TaskMappingProfile : Profile
    {
        public TaskMappingProfile()
        {
            CreateMap<TaskItem, TaskDto>()
                .ForMember(d => d.Author, map => map.MapFrom(s => s.Author))
                .ForMember(d => d.Responcible, map => map.MapFrom(s => s.Responcible))
                .ForMember(d => d.History, map => map.MapFrom(s => s.History))
                .ForMember(d => d.Project, map => map.MapFrom(s => s.Project))
                .ForMember(d => d.DeadLine, map => map.MapFrom(s => s.DeadLine.HasValue ? s.DeadLine.Value.ToLocalTime() : (DateTime?)null));

            CreateMap<TaskDto, TaskItem>()
                .ForMember(d => d.Author, map => map.MapFrom(s => s.Author))
                .ForMember(d => d.Responcible, map => map.MapFrom(s => s.Responcible))
                .ForMember(d => d.History, map => map.MapFrom(s => s.History))
                .ForMember(d => d.Project, map => map.MapFrom(s => s.Project))
                .ForMember(d => d.Deleted, map => map.Ignore());

            CreateMap<CreatingTaskDto, TaskItem>()
                .ForMember(s => s.History, map => map.Ignore())
                .ForMember(s => s.Author, map => map.Ignore())
                .ForMember(s => s.DeadLine, map => map.MapFrom(src => src.DeadLine.HasValue 
                                                                        ? src.DeadLine.Value.ToUniversalTime() 
                                                                        : (DateTime?)null)
                );
            CreateMap<UpdatingTaskDto, TaskItem>()
                .ForMember(s => s.History, map => map.Ignore())
                .ForMember(s => s.DeadLine, map => map.MapFrom(src => src.DeadLine.HasValue
                                                                        ? src.DeadLine.Value.ToUniversalTime()
                                                                        : (DateTime?)null)
                ); ;

            CreateMap<TaskHistoryDto, TaskHistory>()
                .ForMember(s => s.User, map => map.MapFrom(t => t.User))
                .ForMember(s => s.Id, map => map.Ignore())
                .ForMember(s => s.Entity, map => map.Ignore());
            CreateMap<TaskHistory, TaskHistoryDto>()
                .ForMember(s => s.OperationDate, map => map.MapFrom(t => t.OperationDate.ToLocalTime()));
        }
    }
}
