﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts.Group;
using Services.Contracts.User;

namespace Services.Implementations.Mapping
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(d => d.UserPic, map => map.MapFrom(s => Convert.FromBase64String(s.UserPic)))
                .ForMember(d => d.Groups, map => map.MapFrom(s => s.Groups.Select(g => new UserGroup { GroupId = g.Id, UserId = g.UsersId })))
                .ReverseMap()
                .ForMember(d => d.UserPic, map => map.MapFrom(s => Convert.ToBase64String(s.UserPic)))
                .ForMember(d => d.Groups, map => map.MapFrom(s => s.Groups.Select(g => new GroupDto { Id = g.GroupId, Name = g.Group.Name, UsersId = g.UserId})));

            CreateMap<SimpleUserDto, User>()
                .ForMember(s => s.Groups, map => map.Ignore())
                .ForMember(s => s.Status, map => map.Ignore())
                .ForMember(s => s.Email, map => map.Ignore())
                .ForMember(s => s.Login, map => map.Ignore())
                .ForMember(s => s.Password, map => map.Ignore())
                .ReverseMap();
        }

    }
}
