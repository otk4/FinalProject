﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts.Project;

namespace Services.Implementations.Mapping
{
    public class ProjectMappingProfile : Profile
    {
        public ProjectMappingProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<CreateProjectDto, Project>()
                .ReverseMap();

            CreateMap<ProjectDto, Project>()
                .ForMember(d => d.Deleted, map => map.Ignore());
        }

    }
}
