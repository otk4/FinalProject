﻿using Domain.Entities;
using Services.Contracts.User;

namespace Services.Contracts.TaskHistory
{
    public class TaskHistoryDto
    {
        public UserDto? User { get; set; }
        public HistoryAction Action { get; set; }
        public string Operation { get; set; }
        public DateTime OperationDate { get; set; }
        public string? Comment { get; set; }
    }
}
