﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.AccessRight
{
    /// <summary>
    /// Dto прав.
    /// </summary>
    public class AccessRightDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ид объекта прав.
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Тип сущности.
        /// </summary>
        public Guid EntityTypeGuid { get; set; }
    }
}
