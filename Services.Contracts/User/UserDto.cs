﻿using Services.Contracts.Group;

namespace Services.Contracts.User
{
    /// <summary>
    /// Дто пользователя.
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Ид чата в Телеграмм
        /// </summary>
        public long ChatId { get; set; }

        /// <summary>
        /// Статус.
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Аватарка юзера
        /// </summary>
        /// <remarks>Base64 string</remarks>
        public string? UserPic { get; set; }

        /// <summary>
        /// Личный телефон
        /// </summary>
        public string? PrivatePhone { get; set; }

        /// <summary>
        /// Рабочий телефон
        /// </summary>
        public string? WorkPhone { get; set; }

        /// <summary>
        /// Группы.
        /// </summary>
        public ICollection<GroupDto> Groups { get; set; }
    }
}
