﻿namespace Services.Contracts.Project
{
    /// <summary>
    /// Дто создаваемого проекта
    /// </summary>
    public sealed record CreateProjectDto
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст.
        /// </summary>
        public string Text { get; set; }
    }
}
