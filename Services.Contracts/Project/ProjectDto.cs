﻿namespace Services.Contracts.Project
{
    public class ProjectDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст.
        /// </summary>
        public string Text { get; set; }
    }
}
