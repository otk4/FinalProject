﻿namespace Services.Contracts.Users
{
    public sealed record GetSimpleUserDto
    {
        /// <summary>
        /// Идентификатор юзера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }
    }
}
