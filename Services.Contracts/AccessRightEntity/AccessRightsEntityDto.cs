﻿using Services.Contracts.AccessRight;
using Services.Contracts.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.AccessRightEntity
{
    /// <summary>
    /// Dto прав на сущности.
    /// </summary>
    public class AccessRightsEntityDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Права доступа.
        /// </summary>
        public AccessRightDto AccessRight { get; set; }

        /// <summary>
        /// Субъект прав.
        /// </summary>
        public UserDto Owner { get; set; }

        /// <summary>
        /// Тип прав.
        /// </summary>
        public int AccessRightType { get; set; }
    }
}
