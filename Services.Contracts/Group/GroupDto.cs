﻿using Services.Contracts.User;

namespace Services.Contracts.Group
{
    /// <summary>
    /// Дто группы.
    /// </summary>
    public class GroupDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Пользователи.
        /// </summary>
        public int UsersId { get; set; }
    }
}
