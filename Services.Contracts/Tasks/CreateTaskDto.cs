﻿namespace Services.Contracts.Tasks
{
    public sealed record CreateTaskDto
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст задачи.
        /// </summary>
        public string ActiveText { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// ID Ответсвенного.
        /// </summary>
        public int ResponcibleId { get; set; }

        /// <summary>
        /// ID Проекта.
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public string State { get; set; }
    }
}
