﻿
namespace Services.Contracts.Task
{
    public class UpdatingTaskDto
    {
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст задачи.
        /// </summary>
        public string ActiveText { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public int ResponcibleId { get; set; }

        /// <summary>
        /// Автор таски.
        /// </summary>
        public int AuthorId { get; set; }

        /// <summary>
        /// Проект, идентификатор
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Срок завершения.
        /// </summary>
        public DateTime? DeadLine { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public string State { get; set; }
    }
}
