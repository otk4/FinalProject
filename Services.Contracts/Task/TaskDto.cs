﻿using Domain.Entities;
using Services.Contracts.Project;
using Services.Contracts.TaskHistory;
using Services.Contracts.User;

namespace Services.Contracts.Task
{
    public class TaskDto
    {
        /// <summary>
        /// Ид.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Текст задачи.
        /// </summary>
        public string ActiveText { get; set; }

        /// <summary>
        /// Приоритет.
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// Автор.
        /// </summary>
        public UserDto Author { get; set; }

        /// <summary>
        /// Ответсвенный.
        /// </summary>
        public UserDto Responcible { get; set; }

        /// <summary>
        /// Проект.
        /// </summary>
        public ProjectDto Project { get; set; }

        /// <summary>
        /// История работы с задачей.
        /// </summary>
        public List<TaskHistoryDto> History { get; set; }

        /// <summary>
        /// Срок завершения.
        /// </summary>
        public DateTime? DeadLine { get; set; }

        /// <summary>
        /// Состояние.
        /// </summary>
        public string State { get; set; }
    }
}
